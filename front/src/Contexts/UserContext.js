import { createContext, useState } from "react";

export const ThemeContext = createContext("light");

export default function ThemeProvider({ children }) {
  const [userName, setUserName] = useState("sunshine2910");

  return (
    <ThemeContext.Provider value={{ userName, setUserName }}>
      {children}
    </ThemeContext.Provider>
  );
}