// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore } from "firebase/firestore";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyDI0LDhM_JJNReq0wwudOxZ4KrHREfQZHg",
  authDomain: "goldman-ducher-aufrechte-dfae9.firebaseapp.com",
  projectId: "goldman-ducher-aufrechte-dfae9",
  storageBucket: "goldman-ducher-aufrechte-dfae9.appspot.com",
  messagingSenderId: "487052663399",
  appId: "1:487052663399:web:122e284f90afe24cda92c2",
  measurementId: "G-PTL60296M1"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getFirestore(app);

const auth = getAuth(app);

export { app, auth, db };
