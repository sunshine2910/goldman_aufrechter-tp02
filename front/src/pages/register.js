import { useCallback, useState } from "react";
import { createUserWithEmailAndPassword } from "firebase/auth";
import { auth, db } from "../firebase";
import { useNavigate } from "react-router-dom";
import { doc, setDoc } from "firebase/firestore";

export default function Register(){
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [error, setError] = useState("");
    const navigate = useNavigate();
    const [pseudo, setPseudo] = useState("");
	const handlePseudo = useCallback((e) => {
		setPseudo(e.target.value);
	}, [setPseudo]);

    const handleEmailChange = useCallback((e) => {
        setEmail(e.target.value);
    }, [setEmail]);
    const handlePasswordChange = useCallback((e) => {
        setPassword(e.target.value);
    }, [setPassword]);

    const handleSubmit = useCallback((e) => {
        e.preventDefault();
        createUserWithEmailAndPassword(auth, email, password)
            .then((userCredential) => {
    // Signed in 
    // eslint-disable-next-line
            const user = userCredential.user;
        console.log(userCredential.user);
        setDoc(doc(db, "users", pseudo, "messages"), {pseudo: pseudo});
        navigate("/login");
        })
        .catch((error) => {
        setError(error.message);
     });
    }, [email, password]);
    return (
        <div>
            <h1>Register</h1>
            <form onSubmit={handleSubmit}>
                <label htmlFor="email">Email</label>
                <input type="email" id="email" onChange={handleEmailChange} />
                <label htmlFor="password">Password</label>
                <input type="password" id="password" onChange={handlePasswordChange} />
                <label htmlFor="pseudo">Pseudo</label>
				<input type="pseudo" id="pseudo" onChange={handlePseudo} />
                <button type="submit">Login</button>
                {error && <p>{error}</p>}
            </form>
        </div>
    );
}

